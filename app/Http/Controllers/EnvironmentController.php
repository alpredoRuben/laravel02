<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EnvironmentController extends Controller
{
    protected $INTERNAL_CODE = 'CL096';
    private $agent_id = 'ELOK';
    private $login_name = 'ELOK';
    private $password = 'ELOKXML1118';

    public function XMLGetDetailHotel($hotel_id) {
        $str ='<?xml version="1.0" encoding="utf-8"?><Service_GetHotelDetail></Service_GetHotelDetail>';
        $str_xml = new \SimpleXMLElement($str);
        $array_data = array(
            'AgentLogin' =>array(
                'AgentId' => $this->agent_id,
                'LoginName' => $this->login_name,
                'Password' => $this->password,
            ),
            'GetHotelDetail_Request' => array(
                'HotelId' => $hotel_id
            )
        );

        $this->convertArrayToXML($array_data, $str_xml);
        return $str_xml->asXML();
    }

    public function convertArrayToXML($array, &$xmlParent) {
        foreach($array as $key => $value) {
            if(is_array($value)) {
                if(!is_numeric($key)){
                    $subnode = $xmlParent->addChild("$key");
                    $this->convertArrayToXML($value, $subnode);
                }else{
                    $subnode = $xmlParent->addChild("item$key");
                    $this->convertArrayToXML($value, $subnode);
                }
            }else {
                $xmlParent->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }

}
