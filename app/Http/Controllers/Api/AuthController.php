<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

use App\User;
use JWTAuth;

class AuthController extends Controller
{

    /** @var RESPONSE_JSON */
    protected function  setResponse($bools=true, $msg=null, $opt=null, $code=200) {
        $data = array(
            'success' => $bools,
            'message' => ($msg!=null) ? $msg : 'No result response data messages'
        );

        if($opt != null)
            $data['optional'] = $opt;

        return response()->json($data, $code);
    }

    /** @method LOGIN - POST */
    public function login(Request $req) {

        $validate = Validator::make($req->all(), [
            'userid' => 'required',
            'password' => 'required',
        ]);

        if($validate->fails()){
            return $this->setResponse(false, $validate->messages(), $validate->errors(), 401);
        }

        $user = User::with('rolesJoin')->where('username', $req->get('userid'))->orWhere('email', $req->get('userid'))->first();

        if($user && Hash::check($req->get('password'), $user->password)) {

            $credentials = ['email' => $user->email, 'password' => $req->get('password')];
            $token = JWTAuth::attempt($credentials);

            try{
                if(!$token) {
                    return $this->setResponse(false, 'We cant find an account with this credentials. Please make sure you entered the right email and password.', null, 404);
                }
            }
            catch(JWTException $e) {
                return $this->setResponse(false, 'Failed to login. Please try again', $e, 500);
            }

            // Session::put('user_id',$user->id);
            // Session::put('user_pwd',$user->password);
            // Session::put('user_data', $user);
            // Session::put('state_login',TRUE);

            return $this->setResponse(true,'Login successfully', [
                'user_token_value' => $token,
                'user_data_value' => $user
            ], 200);


        }
        return $this->setResponse(true,'Login failed. Please check username or email and password your account',null, 200);
    }

    /** @method REGISTRATION - POST */
    public function registration(Request $req) {

        $credentials = $req->only('email', 'password');

        $validate = Validator::make($req->all(), [
            'username' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|max:50'
        ]);

        if($validate->fails())
            return $this->setResponse(false, $validate->messages(), $validate->errors(), 401);

        $user = User::create([
            'roles_id'  => $req->get('register_id'),
            'username'  => $req->get('username'),
            'name'      => $req->get('fullname'),
            'email'     => $req->get('email'),
            'password'  => Hash::make($req->get('password'))
        ]);

        $token = JWTAuth::fromUser($user);

        return $this->setResponse(
            true,
            'Thank you '.$req->get('username').'. You have registrated your account successfully',
            compact('user','token')
        );

    }

    /** @method LOGOUT - GET */
    public function logout(Request $req) {
        $token = $req->header('Authorization');

        try {
            JWTAuth::invalidate($token);
            return $this->setResponse(true, 'Logout successfully', null, 200);
        }
        catch (JWTException $e) {
            return $this->setResponse(false, 'Failed to logout. Something wrong. Please try again', $e, 500);
        }
    }

    public function recover() {

    }

    public function verificated() {

    }
}

//eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9zaWduX3VwIiwiaWF0IjoxNTM2NzIyNjcxLCJleHAiOjE1MzY3MjYyNzEsIm5iZiI6MTUzNjcyMjY3MSwianRpIjoidUxYVFMySXVwaWt0cnRNYSIsInN1YiI6MSwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.ksH4-rfwvsEVHnFM-C5orSv1wd7dkUEmPE1nV_Ntaa4
