<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\EnvironmentController;
use App\Helpers\HotelExecute;

class TestController extends EnvironmentController
{
    public $hotel_helper;

    public function __construct() {
        $this->hotel_helper = new HotelExecute();
    }

    public function requestConnect() {
        //$xml = $this->XMLGetDetailHotel('WSMA0511000113');
        //echo $xml;
        $xml = 'requestXML=<?xml version="1.0" encoding="utf-8" ?>
        <Service_GetHotelDetail>
        <AgentLogin>
        <AgentId>ELOK</AgentId>
        <LoginName>ELOK</LoginName>
        <Password>ELOKXML1118</Password>
        </AgentLogin>
        <GetHotelDetail_Request>
        <HotelId>WSASTHBKK000087</HotelId>
        </GetHotelDetail_Request>
        </Service_GetHotelDetail>';

        $result = $this->hotel_helper->curlPostHotelDetail($xml);
        return response()->json([$result], 200);
    }



}
