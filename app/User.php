<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $table = 'users';

    protected $primaryKey = 'id';

    protected $fillable = [
        'roles_id', 'username', 'name', 'email', 'password'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rolesJoin() {
        return $this->belongsTo(Role::class, 'roles_id', 'id');
    }

    /* Get the identifier that will be stored in the subject claim of the JWT */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /* Return a key value array, containing any custom claims to be added to the JWT */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /** Menambahkan role (hak akses) baru pada user */
    public function putRole($roles_name) {
        if(is_string($roles_name)){
            $roles_name = Role::where('roles_name', $roles_name)->first();
        }

        return $this->rolesJoin()->attach($roles_name);
    }

    /* Menghapus role (hak akses) pada user*/
    public function forgetRole($roles_name) {
        if(is_string($roles_name)) {
            $roles_name = Role::where('roles_name', $roles_name)->first();
        }

        return $this->rolesJoin()->detach($roles_name);
    }

    /* Mengecek apakah user yang sedang login punya hak akses untuk mengakses page sesuai rolenya */
    public function hasRole($roles_name)
    {
        foreach ($this->rolesJoin as $r)
        {
            if ($r->roles_name === $roles_name)
                return true;
        }
        return false;
    }
}
