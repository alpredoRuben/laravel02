<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    protected $primaryKey = 'id';

    protected $fillable = ['roles_name'];

    public $timestamps = false;

    public function userJoin() {
        return $this->hasMany(User::class, 'roles_id', 'id');
    }
}
