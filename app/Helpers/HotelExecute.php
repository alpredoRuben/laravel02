<?php

namespace App\Helpers;

class HotelExecute {

    protected $URL_BOOK_HOTEL           = 'http://209.58.183.108/xml/book';
    protected $URL_VIEW_CANCEL_POLICY   = 'http://209.58.183.108/xml/viewpolicy';
    protected $URL_SEARCH_HOTEL         = 'http://209.58.183.108/xml/search';
    protected $URL_GET_HOTEL_DETAIL     = 'http://jcho.travflex.com/11WS/ServicePHP/GetHotelDetails.php';
    protected $URL_CANCEL_RESERVATION   = 'http://209.58.183.108/xml/cancel';
    protected $URL_SEARCH_AMEND_HOTEL   = 'http://jcho.travflex.com/11WS_SP2_1/ServicePHP/SearchAmendHotel.php';
    protected $URL_GET_CANCEL_POLICY    = 'http://jcho.travflex.com/11WS_SP2_1/ServicePHP/GetCancelpolicy.php';
    protected $URL_GET_RSVN_INFO        = 'http://jcho.travflex.com/11WS_SP2_1/ServicePHP/GetRSVNInfo.php';


    public function curlPostHotelDetail($xml_data) {

        $headers = array(
            "Content-type: text/xml",
            "Content-length: " . strlen($xml_data),
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->URL_GET_HOTEL_DETAIL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "requestXML=".$xml_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);

        $data = curl_exec($ch);

        if(curl_errno($ch)){
            return curl_error($ch);
        }

        curl_close($ch);
        $array_data = json_decode(json_encode(simplexml_load_string($data)), true);
        return $array_data;
    }



}
