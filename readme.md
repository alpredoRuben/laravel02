## Install and Use Project Laravel 02

Untuk menggunakan project laravel02 ini, tahapan yang dilakukan adalah sebagai berikut :

- git clone (https://gitlab.com/alpredoRuben/laravel02.git) [project_name_optional]
- cd laravel02/[project_name_optional]
- composer update.
- php artisan migrate.
- php artisan serve.

##  Penggunaan JWTAuth Di Laravel

### *Langkah Pertama*
Tambahkan **tymon/jwt-auth: "1.0.0-rc.1"** pada file **composer.json** seperti di bawah ini :
```
    "require": {
        "php": "^7.1.3",
        "fideloper/proxy": "^4.0",
        "laravel/framework": "5.6.*",
        "laravel/tinker": "^1.0",
        "tymon/jwt-auth": "1.0.0-rc.2"
    }
```

### *Langkah Kedua*
Tambahkan **JWT Provider dan Facades** di **config/app.php** item array provider. Lihat contoh berikut
```
    'providers' => [
        ...
        Tymon\JWTAuth\Providers\LaravelServiceProvider::class,
        ...
    ],

    'aliases' => [
        ...
            'JWTAuth' => Tymon\JWTAuth\Facades\JWTAuth::class, 
            'JWTFactory' => Tymon\JWTAuth\Facades\JWTFactory::class
        ...
    ]
```

### *Langkah Ketiga*
Publish provider dengan perintah seperti di bawah ini:
```
    php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
```

### *Langkah Keempat*
Selanjutnya membuat file **jwt.php** pada folder **config** dengan perintah berikut ini
```
    php artisan jwt:secret
```

### *Langkah Kelima*
Registrasi **jwt.auth** dan **jwt.refresh middleware** pada file **app/http/Kernel.php**
```
    protected $routeMiddleware = [
        ...
        'jwt.auth' => \Tymon\JWTAuth\Middleware\GetUserFromToken::class,
        'jwt.refresh' => \Tymon\JWTAuth\Middleware\RefreshToken::class
    ];

```
### *Langkah Keenam (Optional)*
Selanjutnya pengaturan route di file **routes/api.php**
```
    Route::post('your_register_link', 'ControllerName@registerMethod');
    Route::post('your_login_link', 'ControllerName@loginMethod');
    Route::post('your_recover_link', 'ControllerName@recoverMethod');

    Route::group(['middleware' => ['jwt.auth']], function() {
        Route::get('logout', 'ControllerName@logoutMethod');
        Route::get('your_url', 'yourcontroller@controllerMethod');
    });
```

### *Langkah Ketujuh*
Pengaturan route di file **routes/web.php**
```
    Route::get('user/verify/{verification_code}', 'ContrllerName@varificated');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.request');
    Route::post('password/reset', 'Auth\ResetPasswordController@postReset')->name('password.reset');
```


