@extends('auth/AuthLayout')

@section('title')
    SIGN IN | LARAVEL-02
@endsection


@section('content')
<div class="signup-form">
    <form action="" method="post">
        <h2>Sign In</h2>
        <p class="hint-text">Login To Your Account</p>
        <div class="form-group">
            <input type="text" class="form-control" name="username" placeholder="Username" required="required">
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="Password" required="required">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success btn-lg btn-block">Login</button>
        </div>
    </form>
    <div class="text-center">Do you want to create new account. Please <a href="#">Sign Up</a></div>
</div>
@endsection
