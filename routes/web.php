<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** Verifikasi Password */
// Route::get('user/verify/{code}', 'Api\AuthController@verificated');
// Route::get('user/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.request');
// Route::post('user/password/reset', 'Auth\ResetPasswordController@postReset')->name('password.reset');


Route::get('/req/connect', 'TestController@requestConnect');

